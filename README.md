# Simple NetEaseMusic for linux 
目前，音乐品质和网页版的品质一样。
要使用高品质音乐，本来是需要付费的，所以这里是没有的

## 依賴
Python 2.7, PyQt4, PyQt4-phonon

**Ubuntu下**

```
sudo apt-get install python-qt4
sudo apt-get install python-qt4-phonon
```

### v0.1 截图
![登陆](http://static.oschina.net/uploads/code/201503/13014648_wzTA.png)
![播放时](http://static.oschina.net/uploads/code/201503/13014648_ePj4.png)

UBuntu下要生成桌面图标也很简单,可以参考neteasemusic.desktop这个文件，自己修改路径，然后chmod +x, 把这个文件放入home目录下的desktop文件夹中

此作品仅供学习参考，如有任何侵权，请通知作者。
email: yinshaowen241\[at\]gmail\[dot\]com
